# fancy 99 bottles of beer

A '99 Bottles of beer' program with some extra stuff


| Command | |
|---|---|
|-l *[string]* |Changes from beer to what you've typed|
|-f| Fibonacci mode. Every step is a Fibonacci number, changes default start value to 11 (89 after operation)|
|-b| FizzBuzz mode. If number is divisible by 3, it'll be 'Fizz', if by 5, 'Buzz', both 'FizzBuzz' |
|-m *[number]* |Changes the maximum/start number, overrides '-f' maximum value, number must be positive integer|

#include <iostream>
#include <string>
#include <vector>

/*
    '99 Bottles of beer generator', by Waces Ferpit, some day in 2019 out of boredom
*/

std::string btl_(int i_){
	if (i_ == 1){
		return "bottle ";
	}
	else{
		return "bottles ";
	}
}

std::string ono(int i_){
	if (i_ == 0){
		return "no ";
	}
	else{
		return std::to_string(i_) + " ";
	}
}

long unsigned int fib(int i_);
std::string fbz(int i_);

std::string fon(std::string st, int i_, bool bm){
	if (bm){
		return fbz(i_);
	}
	else{
		return st;
	}
}

std::string ot(int i_, bool fm, int ps){
	if (i_ == 1){
		return "it ";
	}
	else{
		if (fm){
			return  std::to_string(i_-fib(ps-1)) + " ";
		}
		else{
			return "one ";
		}
	}
	
}

std::string it(int i_){
	if (i_ == 1){
		return " it";
	}
	else{
		return " them";
	}
}

void err_0();
void help_();

int max_ = 99;
bool fib_mode = false;
bool fz_m = false;
std::string lqd = "beer";
int main(int argc, char *argv[]){
	
	std::vector<std::string> rgs;
	for (int i = 1; i < argc; i++){
		rgs.push_back(argv[i]);
	}
	for (unsigned int i = 0; i < rgs.size() ; i++){
		if (rgs[i] == "-h"){
			help_();
		}
    	if (rgs[i] == "-l"){
			lqd = rgs[i+1];
		}
		if (rgs[i] == "-f"){
			fib_mode = true;
			max_ = 10;
		}
		if (rgs[i] == "-m"){
			try{
				max_ = std::stoi(rgs[i+1]);
			}
			catch(...){
				err_0();
			}
			if (max_ < 1){
				err_0();
			}
		}
    	if (rgs[i] == "-b"){
			fz_m = true;
		}
		
	}	
	for (int i = max_; i > 0; i--){
		
		int i_ = i;
		if (fib_mode){
			i_ = fib(i);
		}
		int i__ = i-1;
		if (fib_mode){
			if ( !(i_ == 1) ){
				i__ = fib(i-1);
			}
			else{
				i__ = 0;
			}
		}
		
		std::cout << fon(ono(i_),i_,fz_m) << btl_(i_) << "of " << lqd << ' ' << "on the wall, " << fon(ono(i_),i_,fz_m) << btl_(i_)
		<< "of " << lqd << ".\nTake "<< ot(i_, fib_mode, i) << "down, pass" << it(i_-i__) << " around, "
		<< fon(ono(i__),i__,fz_m) << btl_(i__) << "of " << lqd << " on the wall." << "\n\n";
	}
	
	int max__ = max_;
	if (fib_mode){
		max__ = fib(max_);
	}
	
	std::cout << "No bottles of " << lqd << " on the wall, no bottles of "<< lqd
	<<".\nGo to the store and buy some more, " << fon(std::to_string(max__),max__,fz_m) << btl_(max__)
	<< "of "<< lqd <<" on the wall." << '\n';

}

long unsigned int fib(int i_){
	unsigned int fb[] =  {1, 0};
	bool zo = true; // (int)true = 1, (int)false = 0
	for (int i = 1; i < i_; i++){
		fb[ (int)zo ] = fb[0] + fb[1];
		zo = !zo;
	}
	return fb[0] + fb[1];
}

std::string fbz(int i_){
	std::string fbz = "";
	if ( i_%3 == 0 ){
		fbz = fbz + "Fizz";
	}
	if ( i_%5 == 0){
		fbz = fbz + "Buzz";
	}
	if ( (i_%3 != 0) && (i_%5 != 0)  ){
		fbz = std::to_string(i_);
	}
	if (i_ == 0){
		fbz = "no";
	}
	fbz = fbz + " ";
	return fbz;
}

void err_0(){
	std::cout << "Item after '-m' must be an integer of value bigger than 0." << '\n';
	std::exit(1);
}

void help_(){
	std::cout << "'99 Bottles of beer' song generator.\n"
	<< " -l <string>\tChanges from beer to what you've typed\n"
	<< " -f\t\tFibonacci mode. Every step is a fibonacci number, changes default start value to 11 (89 after operation)\n"
	<< " -b\t\tFizzBuzz mode. If number is divisible by 3, it'll be 'Fizz', if by 5, 'Buzz', both 'FizzBuzz'\n"
	<< " -m <number>\tChanges the maximum/start number, overrides '-f' maximum value, number must be positive integer\n"
	<< " -h\t\tShows this help\n";
	std::exit(0);
}
